# README #

Share repository

### What is this repository for? ###

* This repository is for share information.

### Files ###

* GRIN-Global 1.9.3 backup for testing interaction with Genesys. 12th November. The Get_Passport_data was modify according with the FAO_MCPD_2012.

* gringlobal_1_9_3_empty.bak(SQL Server format). GRIN Global database backup without data. This backup should be used as “source” database in order to verify that the all dataviews from versions 1.7 and 1.9.1 were updated to version 1.9.3.